package com.hcl.beans;

public class Books {
	
	private int bookId;
	private String title;
	private String genre;
	
	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Books(int bookId, String title, String genre) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.genre = genre;
	}
            // setters are used to set values for variables
		
		//getters are used to retrieve the stored values
		

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	

}

