package com.hcl.beans;
public class Login {
	 private long mobileNumber;
	 private String email;
	 private String userName;
	 private String password;
	 
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Login(long mobileNumber, String email, String userName, String password) {
		super();
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.userName = userName;
		this.password = password;
	}
            // setters are used to set values for variables
		
		//getters are used to retrieve the stored values
		 
	
	public long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	  

}

